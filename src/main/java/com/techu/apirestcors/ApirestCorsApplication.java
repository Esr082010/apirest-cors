package com.techu.apirestcors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApirestCorsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApirestCorsApplication.class, args);
	}

}
