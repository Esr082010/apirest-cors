package com.techu.apirestcors;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST })

public class HolaMundoController {
    @GetMapping("/saludos")
    public String saludar(){
        return "Hola CORS!";
    }
}
